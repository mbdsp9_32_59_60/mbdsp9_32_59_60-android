package com.example.mbdsp9_32_59_60_android.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.mbdsp9_32_59_60_android.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}